#!/bin/bash

export PYTHONPATH=$HOME

set_title()
{
    echo -e '\[\e]0;'"$@"'\a\]'
}

set_title vm86
#~/auto/bin/wmctrl.py -p c0 m0
#wmctrl -ir $(printf "0x%x\n" `xdotool getactivewindow`) -b add,maximized_vert,maximized_horz
exec tmux new ~/auto/vm/freebsd-x86.sh
