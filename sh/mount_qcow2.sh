#!/bin/sh

if [ $# -ne 1 ]; then
    echo "usage: $0 <qcow2_file>"
    exit 1
fi

if ! lsmod | grep -q nbd; then
    sudo modprobe nbd max_part=8
fi

sudo qemu-nbd --connect=/dev/nbd0 "$1"
