#!/bin/bash

export PYTHONPATH=$HOME

set_title()
{
    echo -e '\[\e]0;'"$@"'\a\]'
}

set_title bsd86_vim
~/auto/bin/wmctrl.py -p c1 m1
wmctrl -ir $(printf "0x%x\n" `xdotool getactivewindow`) -b toggle,maximized_vert,maximized_horz
exec ssh -o NoHostAuthenticationForLocalhost=yes -p 1401 localhost
