#!/bin/bash

export PYTHONPATH=$HOME

set_title()
{
    echo -e '\[\e]0;'"$@"'\a\]'
}

set_title bsd86
~/auto/bin/wmctrl.py -p c0 m0
wmctrl -ir $(printf "0x%x\n" `xdotool getactivewindow`) -b toggle,maximized_vert,maximized_horz
exec ssh -CYA -o NoHostAuthenticationForLocalhost=yes -p 1401 localhost
