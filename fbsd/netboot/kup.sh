#!/bin/bash

set -exu

SHOME=/home/$SUDO_USER

cd $SHOME/netboot

sudo mount $SHOME/disks/fbsd13-ppc64-fat.raw /mnt/tmp
sudo cp kernel /mnt/tmp/boot/kernel/kernel
sudo umount /mnt/tmp
