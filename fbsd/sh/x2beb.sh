#!/bin/sh
#
# heX to Big-Endian Bit

if [ $# -lt 1 ]; then
	echo "usage: $0 <hex>"
	exit 1
fi
x=$1

for i in `seq 0 63`; do
	if [ $((x)) -eq $((1<<i)) ]; then
		echo $((63-i))
		exit
	fi
done

echo "ERROR"
