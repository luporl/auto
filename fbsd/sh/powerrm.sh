#!/bin/sh

if ! sudo rm -rf "$@"; then
	sudo chflags -R 0 "$@"
	sudo rm -rf "$@"
fi
