#!/bin/sh

echo 'root:passwd' > /secrets
chmod 640 /secrets

cat >/rsyncd.conf <<EOF
port = 22

[tmp]
	path = /tmp
	auth users = root:rw
	secrets file = /secrets
EOF

rsync --daemon --config=/rsyncd.conf
