REL_DIR		:= /usr/obj/usr/src/powerpc.powerpc64/release
VMIMG_DIR	:= /ufs/release/vmimg
VMIMG_UFS_DIR	:= $(VMIMG_DIR)/ufs
VMIMG_UFS	:= $(VMIMG_DIR)/ufs.img
VMIMG		:= $(VMIMG_DIR)/disk.qcow2
STAMPS		:= $(VMIMG_DIR)/stamps
VMIMG_SIZE	:= 8g
#VMIMG_SIZE	:= 20g
SWAP_SIZE	:= 2G
USE_BSD_SLICE	:= 0

#ROOT_DEV	:= da0
ROOT_DEV	:= vtbd0
#NETIF		:= llan0
NETIF		:= vtnet0

all: $(VMIMG)

CHECK_ROOT	:= if [ x`whoami` != xroot ]; then \
			echo "This script must be run as root"; \
			exit 1; \
		fi

clean:
	$(CHECK_ROOT)
	rm -rf $(VMIMG_DIR) || chflags -R 0 $(VMIMG_DIR)
	rm -rf $(VMIMG_DIR)
	mkdir -p $(VMIMG_UFS_DIR)
	mkdir -p $(STAMPS)

$(STAMPS)/extract:
	$(CHECK_ROOT)
	# Extract base packages
	if [ ! -f $(VMIMG_UFS_DIR)/bin/ls ]; then \
		for f in base kernel; do \
			tar xf $(REL_DIR)/$${f}.txz -C $(VMIMG_UFS_DIR); \
		done; \
	fi
	touch $@

extract: $(STAMPS)/extract

$(STAMPS)/cfg:
	$(CHECK_ROOT)
	# Setup fstab
	f=$(VMIMG_UFS_DIR)/etc/fstab && \
	echo '# Device        Mountpoint      FStype  Options Dump    Pass#' >$$f && \
	if [ x$(USE_BSD_SLICE) = x1 ]; then \
		echo '/dev/$(ROOT_DEV)s2b none            swap    sw      0       0' >>$$f && \
		echo '/dev/$(ROOT_DEV)s2a /               ufs     rw      1       1' >>$$f; \
	else \
		echo '/dev/$(ROOT_DEV)s2 none            swap    sw      0       0' >>$$f && \
		echo '/dev/$(ROOT_DEV)s3 /               ufs     rw      1       1' >>$$f; \
	fi
	# Setup rc.conf
	echo 'hostname="fbsd13"' >$(VMIMG_UFS_DIR)/etc/rc.conf
	echo 'ifconfig_$(NETIF)="DHCP"' >>$(VMIMG_UFS_DIR)/etc/rc.conf
	echo 'sshd_enable="YES"' >>$(VMIMG_UFS_DIR)/etc/rc.conf
	# Create entropy
	dd if=/dev/random of=$(VMIMG_UFS_DIR)/boot/entropy bs=4k count=1
	touch $@

cfg: $(STAMPS)/cfg

$(VMIMG_UFS): $(STAMPS)/extract $(STAMPS)/cfg
	# Make UFS img
	# 2^17 (131072) / 2^16 (65536) inodes per GB
	s=`echo $(VMIMG_SIZE) | sed s/g//` && \
	i=`echo $$((65536*$$s))` && \
	makefs -B be -t ffs -s $(VMIMG_SIZE) \
		-f "$$i" \
		-o version=2,softupdates=1,bsize=32768,fsize=4096 \
		$(VMIMG_UFS) $(VMIMG_UFS_DIR)

ufs: $(VMIMG_UFS)

# Note: BSD slices container is not working when cross created from amd64.
#       As workaround, add UFS image directly on MBR partition  #2
$(VMIMG): $(VMIMG_UFS)
	if [ x$(USE_BSD_SLICE) = x1 ]; then \
		mkimg -a1 -s mbr -f qcow2 \
			-p prepboot:=$(VMIMG_UFS_DIR)/boot/boot1.elf \
			-p freebsd:-"mkimg -s bsd -p freebsd-ufs:=$(VMIMG_UFS) -p freebsd-swap::$(SWAP_SIZE)" \
			-o $(VMIMG); \
	else \
		mkimg -a1 -s mbr -f qcow2 \
			-p prepboot:=$(VMIMG_UFS_DIR)/boot/boot1.elf \
			-p freebsd::$(SWAP_SIZE) \
			-p freebsd:=$(VMIMG_UFS) \
			-o $(VMIMG); \
	fi
	@echo "Success!"

img: $(VMIMG)

test:
	@echo test

.PHONY: clean extract cfg ufs img test
