#!/bin/sh
#
# heX to Big-Endian bit Range

DIR=$(dirname $0)

usage()
{
	echo "usage: $0 <hex> [v]"
	exit 1
}

if [ $# -lt 1 ]; then
	usage
fi
x=$1
v=0
if [ $# -gt 1 ]; then
	if [ "$2" != v ]; then
		usage
	fi
	v=1
fi

first=
last=

for i in `seq 63 0`; do
	b=$((1<<i))
	if [ $((x & b)) -ne 0 ]; then
		bit=`$DIR/x2beb.sh $b`
		if [ $v -eq 1 ]; then
			echo -n "$bit "
		fi
		if [ -z "$first" ] || [ $bit -lt $first ]; then
			first=$bit
		fi
		if [ -z "$last" ] || [ $bit -gt $last ]; then
			last=$bit
		fi
	fi
done
if [ $v -eq 1 ]; then
	echo
fi

echo "$first:$last"
