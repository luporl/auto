#!/bin/sh
#
# Big-Endian bit Range to heX

DIR=$(dirname $0)

width=64
if [ $# -lt 2 ]; then
	echo "usage: $0 <firstBit> <lastBit> [width]"
	echo
	echo "width(default) = $width"
	exit 1
fi
first=$1
last=$2
if [ $# -gt 2 ]; then
	width=$3
fi

acc=0
for i in `seq $first $last`; do
	acc=$((acc + `$DIR/beb2x.sh $i $width`))
done

printf "0x%0$((width/4))X\n" $acc
