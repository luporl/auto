#!/bin/sh
#
# Big-Endian Bit to heX

width=64
if [ $# -lt 1 ]; then
	echo "usage: $0 <bit> [width]"
	echo
	echo "width(default) = $width"
	exit 1
fi
bit=$1
if [ $# -gt 1 ]; then
	width=$2
fi

printf "0x%0$((width/4))X\n" $((1<<(width-1-bit)))
