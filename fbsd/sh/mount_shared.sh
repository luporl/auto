#!/bin/sh
# mount a shared folder, from a QEMU VM, on the host

sudo mount -t 9p -o trans=virtio,version=9p2000.L hostshare /mnt/shared
