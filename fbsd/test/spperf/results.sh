cd /tmp/spperf || exit 1

sudo pmcstat -R spperf.out -g
#sudo pmcstat -R spperf.out -G spperf.callchain

sudo mv INSTR_COMPLETED/kernel.gmon . &&
gprof `sysctl -qn kern.bootfile` kernel.gmon | sudo tee kernel.report >/dev/null &&
sudo rm -rf INSTR_COMPLETED/
