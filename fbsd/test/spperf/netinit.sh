if [ ! -f /tmp/inited ]; then

# warmup
sudo mount -t tmpfs tmpfs /tmp || exit 1
~/test.sh

scp vm86:kernel.full vm86:hwpmc.ko .
sudo sysctl kern.bootfile=$HOME/kernel.full
sudo kldload ./hwpmc.ko

touch /tmp/inited

fi
