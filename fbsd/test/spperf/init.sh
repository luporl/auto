if [ ! -f /tmp/inited ]; then

# warmup
sudo mount -t tmpfs tmpfs /tmp || exit 1
~/test.sh

sudo kldload hwpmc &&
touch /tmp/inited

fi
