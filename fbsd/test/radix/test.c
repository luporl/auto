#include <fcntl.h>
#include <setjmp.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sys/mman.h>
#include <sys/sysctl.h>
#include <sys/types.h>

/* defines */

#define	PAGE_SIZE	4096
#define	SP_SIZE		(512*PAGE_SIZE)

/* promote */
#define	F_PRO		0
#define	F_PRW		1
/* open */
#define	F_ORO		0
#define	F_ORW		8

#define	F_RO		(F_ORO|F_PRO)
#define	F_RW		(F_ORW|F_PRW)
#define	F_ORW_PRO	(F_ORW|F_PRO)	/* open rw, promote ro */
#define	F_DBG		2
#define	F_ANON		4

#define	T_OK		0
#define	T_FAIL		1

#define	TRUE		1
#define	FALSE		0

#define	P_SP_WRFAULT	1
#define	P_WRFAULT	2

#define	FLOG(fmt, ...)							\
	do {								\
		printf("%s: " fmt "\n", __func__, ## __VA_ARGS__);	\
		fflush(stdout);						\
	} while (0)

#define	PRINTFLUSH(fmt, ...)						\
	do {								\
		printf(fmt, ## __VA_ARGS__);				\
		fflush(stdout);						\
	} while (0)

#define	CHECK_COND(cond)						\
	do {								\
		if (!(cond)) {						\
			PRINTFLUSH("%s:%d: CHECK_COND(" #cond		\
			    ") failed\n",				\
			    __func__, __LINE__);			\
			return (T_FAIL);				\
		}							\
	} while (0)

#define	CHECK_RC(a)	CHECK_COND(a == T_OK)

/* types */

typedef unsigned long ulong;

struct strfn {
	const char *str;
	int (*fn)(int);
	int flags;
};

/* data */

static const char *prog;

static ulong sp_promotions0;
static ulong sp_promotions1;
static ulong sp_promotions;
static ulong sp_mappings0;
static ulong sp_mappings1;
static ulong sp_mappings;
static ulong sp_demotions0;
static ulong sp_demotions1;
static ulong sp_demotions;

/* functions */

static unsigned long
sysctl_ulong(const char *id)
{
	unsigned long v;
	size_t len = sizeof(v);

	if (sysctlbyname(id, &v, &len, NULL, 0) == -1) {
		perror("sysctl_ulong");
		exit(1);
	}
	return (v);
}

static void
sp_stats0(void)
{
	sp_promotions0 = sysctl_ulong("vm.pmap.l3e.promotions");
	sp_mappings0 = sysctl_ulong("vm.pmap.l3e.mappings");
	sp_demotions0 = sysctl_ulong("vm.pmap.l3e.demotions");
}

static void
sp_stats1(void)
{
	sp_promotions1 = sysctl_ulong("vm.pmap.l3e.promotions");
	sp_mappings1 = sysctl_ulong("vm.pmap.l3e.mappings");
	sp_demotions1 = sysctl_ulong("vm.pmap.l3e.demotions");

	sp_promotions = sp_promotions1 - sp_promotions0;
	sp_demotions = sp_demotions1 - sp_demotions0;
	sp_mappings = sp_mappings1 - sp_mappings0;
}

static void
sp_stats_print(void)
{
	PRINTFLUSH("promotions: %lu -> %lu\n",
	    sp_promotions0, sp_promotions1);
	PRINTFLUSH("mappings: %lu -> %lu\n",
	    sp_mappings0, sp_mappings1);
	PRINTFLUSH("demotions: %lu -> %lu\n",
	    sp_demotions0, sp_demotions1);
}

static int
sp_promote_check(const char *func)
{
	int rc;

	if (sp_promotions1 > sp_promotions0) {
		rc = T_OK;
		PRINTFLUSH("%s: OK (promote)\n", func);
	} else if (sp_mappings1 > sp_mappings0) {
		rc = T_OK;
		PRINTFLUSH("%s: OK (enter)\n", func);
	} else {
		rc = T_FAIL;
		PRINTFLUSH("%s: FAIL\n", func);
	}

	return (rc);
}

static void *
sp_mmap_promote(int flags, int *fd)
{
	char *m;
	int sum = 0;
	size_t i, n;
	int anon = flags & F_ANON;
	int orw = flags & F_ORW;
	int prw = flags & F_PRW;

	sp_stats0();

	/* need to open a 16MB mmap file in cur dir */
	if (anon)
		*fd = -1;
	else {
		*fd = open("mmap", orw ? O_RDWR : O_RDONLY);
		if (*fd == -1) {
			perror("open failed");
			return (NULL);
		}
	}

	n = SP_SIZE;
	m = mmap(0, n, PROT_READ | (orw? PROT_WRITE : 0),
		(anon? MAP_ANON : MAP_SHARED) |
		MAP_ALIGNED_SUPER, *fd, 0);
	FLOG("m=%p", m);

	if (m == MAP_FAILED) {
		perror("mmap failed");
		goto err;
	}
	/* if rw, init bytes */
	if (prw)
		/* rw promote happens when last page is written here */
		memset(m, 1, n);

	/* sum bytes */
	for (i = 0; i < n; i++)
		/* ro promote happens when last page is read here */
		sum += m[i];
	FLOG("sum=%d", sum);

	sp_stats1();
	if (sp_promote_check(__func__) != T_OK) {
		sp_stats_print();
		return (NULL);
	}
	return (m);

err:
	if (*fd != -1)
		close(*fd);
	return (NULL);
}

/* tests */

/* test sp promote rw with sbrk */
static int
sp_promote_sbrk(int flags)
{
	char *m;
	int sum = 0;
	intptr_t i, j, n;

	FLOG("START");
	sp_stats0();

	/* alloc 3*SP_SIZE to compensate for unaligned memory */
	n = 3 * SP_SIZE;
	m = sbrk(n);
	if (m == (char*)-1) {
		FLOG("sbrk failed");
		return (T_FAIL);
	}
	/* set all bytes to 1 */
	memset(m, 1, n);
	/* read and sum all bytes */
	for (i = 0, j = 0; i < n; i++, j++) {
		if (j == 4 * 1024) {
			j = 0;
			if (((uintptr_t)&m[i] & 0xffffffUL) == 0xfff000UL)
				FLOG("i=%ld, m=%p", i / (1024 * 1024), &m[i]);
		}
		sum += m[i];
	}
	/* sum should be equal to SP_SIZE */
	FLOG("sum=%d", sum);
	sp_stats1();

	sbrk(-n);

	sp_stats_print();
	return (sp_promote_check(__func__));
}

/* test sp promote with mmap */
static int
sp_promote(int flags)
{
	char *m;
	int fd, rc, sum = 0;
	size_t i, n;

	FLOG("START: flags=%x", flags);
	sp_stats0();

	/* need to open a 2MB mmap file in cur dir */
	fd = -1;
	fd = open("mmap", flags & F_RW ? O_RDWR : O_RDONLY);
	if (fd == -1) {
		perror("open failed");
		return (T_FAIL);
	}

	/* map 1 aligned super page */
	n = SP_SIZE;

	/* orig
	m = mmap(0, n, PROT_READ | (flags & F_RW? PROT_WRITE : 0),
			MAP_SHARED | MAP_ALIGNED_SUPER, fd, 0);
	 */
	m = mmap(0, n, PROT_READ | (flags & F_RW? PROT_WRITE : 0),
			MAP_ANON | MAP_ALIGNED_SUPER, -1, 0);
	FLOG("m=%p", m);

	if (m == MAP_FAILED) {
		perror("mmap failed");
		goto err;
	}
	/* if rw, init bytes */
	if (flags & F_RW)
		/* rw promote happens when last page is written here */
		memset(m, 1, n);
	/* sum bytes */
	for (i = 0; i < n; i++) {
		/* ro promote happens when last page is read here */
		sum += m[i];
	}

	FLOG("sum=%d", sum);
	sp_stats1();

	if (fd != -1) {
		fsync(fd);
		CHECK_RC(munmap(m, n));
		close(fd);
	}

	rc = sp_promote_check(__func__);
	if (rc != T_OK)
		sp_stats_print();
	return (rc);

err:
	if (fd != -1)
		close(fd);
	return (T_FAIL);
}

static int
sp_demote(int flags)
{
	char *m;
	int fd;

	FLOG("START");

	m = sp_mmap_promote(F_RW, &fd);
	if (!m)
		return (T_FAIL);

	sp_stats0();
	fsync(fd);
	sp_stats1();
	CHECK_COND(sp_demotions == 1);

	CHECK_RC(munmap(m, SP_SIZE));
	close(fd);
	FLOG("OK");
	return (T_OK);
}

static int
sp_enter(int flags)
{
	ulong mappings, promotions;

	FLOG("START");
	sp_stats0();
	mappings = sp_mappings0;
	promotions = sp_promotions0;

	CHECK_RC(sp_promote(F_RO | F_DBG));
	CHECK_RC(sp_promote(F_RW | F_DBG));

	sp_stats0();
	CHECK_COND(sp_mappings0 > mappings &&
		sp_mappings0 - mappings > sp_promotions0 - promotions);
	FLOG("OK");
	return (T_OK);
}

static int
sp_remove_write(int flags)
{
	char *m;
	int fd;

	FLOG("START");

	m = sp_mmap_promote(F_RW, &fd);
	if (!m)
		return (T_FAIL);

	FLOG("fsync");
	sp_stats0();
	fsync(fd);
	sp_stats1();
	CHECK_COND(sp_demotions == 1);

	CHECK_RC(munmap(m, SP_SIZE));
	close(fd);
	FLOG("OK");
	return (T_OK);
}

static jmp_buf jbuf;

static void
sp_signal(int sig)
{
	longjmp(jbuf, 1);
}

static int
sp_protect(int flags)
{
	char *m, *p;
	int fd = -1;
	void (*oldsig)(int);
	int val;

	FLOG("START");

	oldsig = signal(SIGSEGV, sp_signal);
	if (oldsig == SIG_ERR) {
		perror("signal");
		goto fail;
	}

	m = sp_mmap_promote(F_RW, &fd);
	if (!m)
		goto fail;

	/* sp protect */
	FLOG("sp protect");
	/* should have ref and mod bits set */
	sp_stats0();
	CHECK_RC(mprotect(m, SP_SIZE, PROT_READ));
	sp_stats1();
	/* ref and mod bits should have been cleared */
	CHECK_COND(sp_demotions == 0);

	val = setjmp(jbuf);
	if (val) {
		CHECK_RC(munmap(m, SP_SIZE));
		fsync(fd);
		close(fd);
		signal(SIGSEGV, oldsig);
		FLOG("OK (caught SIGSEGV)");
		return (T_OK);
	}

	if (flags & P_SP_WRFAULT) {
		/* cause fault */
		FLOG("memset %p (should fault)", m + PAGE_SIZE);
		memset(m + PAGE_SIZE, 0xab, PAGE_SIZE);
		goto fail;
	}

	/* demote and protect */
	FLOG("sp unprotect");
	CHECK_RC(mprotect(m, SP_SIZE, PROT_READ | PROT_WRITE));

	FLOG("protect");
	sp_stats0();
	CHECK_RC(mprotect(m, 3 * PAGE_SIZE, PROT_READ));
	sp_stats1();
	CHECK_COND(sp_demotions == 1);

	/* should be ok */
	p = m + 3 * PAGE_SIZE;
	FLOG("memset %p", p);
	memset(p, 0xbc, PAGE_SIZE);

	/* should fault */
	if (flags & P_WRFAULT) {
		p = m;
		FLOG("memset %p (should fault)", m);
		memset(p, 0xbc, PAGE_SIZE);
		goto fail;
	}

	CHECK_RC(munmap(m, SP_SIZE));
	fsync(fd);
	close(fd);
	signal(SIGSEGV, oldsig);

	FLOG("OK");
	return (T_OK);

fail:
	return (T_FAIL);
}

static int
sp_remove(int flags)
{
	char *m;
	int fd = -1;

	FLOG("START");

	m = sp_mmap_promote(F_RO, &fd);
	CHECK_COND(m != NULL);

	FLOG("sp remove");
	sp_stats0();
	CHECK_RC(munmap(m, SP_SIZE));
	sp_stats1();
	close(fd);
	CHECK_COND(sp_demotions == 0);

	m = sp_mmap_promote(F_RO, &fd);
	FLOG("remove");
	sp_stats0();
	CHECK_RC(munmap(m + PAGE_SIZE, 8 * PAGE_SIZE));
	sp_stats1();
	CHECK_RC(munmap(m, SP_SIZE));
	close(fd);
	CHECK_COND(sp_demotions == 1);

	FLOG("OK");
	return (T_OK);
}

static int
sp_all(int flags)
{
	CHECK_RC(sp_promote(F_RO | F_DBG));
	CHECK_RC(sp_promote(F_RW | F_DBG));
	CHECK_RC(sp_promote_sbrk(0));
	CHECK_RC(sp_demote(0));
	CHECK_RC(sp_enter(0));
	CHECK_RC(sp_remove_write(0));
	CHECK_RC(sp_protect(0));
	CHECK_RC(sp_protect(P_WRFAULT));
	CHECK_RC(sp_protect(P_SP_WRFAULT));
	CHECK_RC(sp_remove(0));
	return (T_OK);
}

static struct strfn g_strfn_map[] = {
	{ "promote_dro", sp_promote, F_RO | F_DBG },
	{ "promote_drw", sp_promote, F_RW | F_DBG },
	{ "promote_ro", sp_promote, F_RO },
	{ "promote_rw", sp_promote, F_RW },
	{ "promote_sbrk", sp_promote_sbrk, 0 },
	{ "demote", sp_demote, 0 },
	{ "enter", sp_enter, 0 },
	{ "remwr", sp_remove_write, 0 },
	{ "prot", sp_protect, 0 },
	{ "prot_wf", sp_protect, P_WRFAULT },
	{ "prot_swf", sp_protect, P_SP_WRFAULT },
	{ "rem", sp_remove, 0 },
	{ "all", sp_all, 0 },

	/* shortcuts */
	{ "d", sp_promote, F_RO | F_DBG },
	{ "D", sp_promote, F_RW | F_DBG },
	{ "r", sp_promote, F_RO },
	{ "w", sp_promote, F_RW },
	{ "s", sp_promote_sbrk, 0 },
	{ NULL, NULL, 0 }
};

static void
help(void)
{
	struct strfn *sf;

	for (sf = g_strfn_map; sf->str; sf++)
		printf("%s\n", sf->str);
	exit(0);
}

static void
usage(void)
{
	printf("usage: %s <test>|--help|-h\n", prog);
	exit(1);
}

int main(int argc, char **argv)
{
	const char *arg;
	struct strfn *sf;
	int rc;

	prog = argv[0];

	if (argc != 2)
		usage();
	arg = argv[1];

	if (strcmp(arg, "--help") == 0 || strcmp(arg, "-h") == 0)
		help();

	for (sf = g_strfn_map; sf->str; sf++)
		if (strcmp(arg, sf->str) == 0)
			break;
	if (sf->str == NULL)
		usage();

	FLOG("SUPERPAGES");
	rc = (*sf->fn)(sf->flags);

	if (rc == T_OK)
		FLOG("OK");
	else
		FLOG("FAIL");
	return (0);
}
