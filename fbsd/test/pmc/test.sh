#!/bin/sh

if [ $# -lt 1 ]; then
	echo "usage: $0 <test#>"
	exit 1
fi
t=$1

if uname -m | grep amd64 >/dev/null; then
	ppc=0
	PMCSTAT=pmcstat
	#PMCSTAT=/tmp/pmcstat.full
	INSTR_COMPLETED=inst_retired.any
	CACHE_LOAD_MISS=mem_load_retired.l2_miss
	INSTRUCTIONS=$INSTR_COMPLETED
	#TLB_INSTR_MISS=itlb_misses.miss_causes_a_walk
	TLB_INSTR_MISS=dtlb_load_misses.miss_causes_a_walk
else
	ppc=1
	PMCSTAT=/tmp/pmcstat.full
	INSTR_COMPLETED=INSTR_COMPLETED
	CACHE_LOAD_MISS=CACHE_LOAD_MISS
	INSTRUCTIONS=instructions
	TLB_INSTR_MISS=TLB_INSTR_MISS
fi

# initial setup

init_x86()
{
	# build module
	export KERNBUILDDIR=/usr/obj/usr/src/amd64.amd64/sys/MYKERNEL
	cd /usr/src/sys/modules/hwpmc &&
	make buildenv
	make

	# build apps
	~/tpmc/build.sh

	# load module
	sudo kldload hwpmc

	# mystress
	make -C ~/tpmc clean all &&
	cp ~/tpmc/mystress.static .
}

init_ppc64()
{
	# on ppc64:
	# boot single user
	cd && ./tinit.sh

	# on x86:
	# build module
	~/tpmc/buildenv.sh
	make
	# build apps
	~/tpmc/build.sh
	# copy to machine:
	~/tpmc/scp_hwpmc.sh

	# on ppc64:
	cd /tmp
	make clean all
	./tload.sh
}

### 1 ###
# -S: system-wide, sampling
if [ $t -eq 1 ]; then
set -x
EV=$INSTR_COMPLETED
cd /tmp &&
rm -rf $EV sample.* ddtmp &&
sync
# system-wide statistical sampling
cd /tmp &&
dd if=/dev/zero of=ddtmp bs=1m count=2048 &
cd /tmp &&
$PMCSTAT -O sample.out -S $EV -l 5 &&

# generate callgraph
$PMCSTAT -R sample.out -G sample.graph &&
# generate gprof(1) compatible profiles from a sample file
$PMCSTAT -R sample.out -g &&

# generate report
cd $EV &&
gprof /boot/kernel/kernel kernel.gmon > kernel.report &&
wc -l kernel.report &&
head /tmp/sample.graph || exit 1
# EXPECTED:
# - several samples
# - writes and uiomove_object among functions that spent most time
echo "1 - OK"

### 2 ###
# -p: process mode, counting
elif [ $t -eq 2 ]; then
set -x
EV=$CACHE_LOAD_MISS
# measure the number of data cache misses every 1 second
$PMCSTAT -d -p $EV stress -c 1 -t 3 >/dev/null &&
$PMCSTAT -d -p $EV stress -c 1 -t 6 >/dev/null &&
$PMCSTAT -d -p $EV stress -m 1 -t 3 >/dev/null &&
$PMCSTAT -d -p $EV stress -m 1 -t 6 >/dev/null || exit 1
# EXPECTED:
# - tests that run for more time should show higher counts
# - stress -m runs should show higher counts
echo "2 - OK"

### 3 ###
# -p: process mode, counting
# -t
elif [ $t -eq 3 ]; then
set -x
EV=$INSTRUCTIONS
# measure instructions retired for all processes named pmcstat use:
# CTRL-C to finish
stress -m 1 --vm-hang 1 -t 10 &
$PMCSTAT -w 1 -t stress -p $EV || exit 1
stress -m 1 --vm-hang 3 -t 10 &
$PMCSTAT -w 1 -t stress -p $EV || exit 1
# EXPECTED:
# - first run should show high counts on every line
# - second run should show some ~2 zero lines and a high count one, then repeat
echo "3 - OK"

### 4 ###
# -p: process mode, counting
# -t
# command: end condition
elif [ $t -eq 4 ]; then
set -x
EV=$CACHE_LOAD_MISS
# measure instructions for processes named stress for a period of 5 seconds
stress -c 1 -t 10 &
$PMCSTAT -t stress -p $EV sleep 3 || exit 1
# EXPECTED:
# - pmcstat exit before stress
# - a reasonable number of events
echo "4 - OK"

### 5 ###
# -s: system mode, counting
elif [ $t -eq 5 ]; then
set -x
EV=$TLB_INSTR_MISS
stress -m 4 --vm-hang 2 -t 15 &
# count instruction tlb-misses on CPUs 0 and 2
$PMCSTAT -w 1 -c 0,2 -s $EV sleep 10 || exit 1
# EXPECTED:
# - a pattern of one line showing a high count and the next a low count
echo "5 - OK"


### 6 ###
# -P: process mode, sampling
elif [ $t -eq 6 ]; then
set -x
#EV=br_misp_retired.all_branches
EV=$INSTR_COMPLETED
#PMCSTAT=pmcstat
rm -rf $EV sample.* &&
cd /tmp &&
#$PMCSTAT -n $((0x100000)) -d -P $EV -O sample.out /tmp/mystress &&
$PMCSTAT -n $((0x100000)) -d -P $EV -O sample.out /tmp/mystress.static &&
#$PMCSTAT -n $((0x100000)) -d -P $EV -O sample.out /tmp/stress.static -c 1 -t 3 &&
#$PMCSTAT -n $((0x100000)) -d -P $EV -O sample.out /tmp/stress -c 1 -t 3 &&
$PMCSTAT -R sample.out -G sample.graph &&
$PMCSTAT -R sample.out -g &&
head sample.graph || exit 1
# EXPECTED:
# - a high number of samples, most resolved correctly
# - graph showing hogcpu among most used functions
echo "6 - OK"


### 7 ###
# -P: process mode, sampling
elif [ $t -eq 7 ]; then
set -x
EV=$INSTR_COMPLETED
cd /tmp && rm -rf $EV sample.*
cd /tmp &&
/tmp/mystress.static &
sleep 1
cd /tmp &&
$PMCSTAT -n $((0x1000000)) -d -P $EV -O sample.out -t mystress &&
$PMCSTAT -R sample.out -G sample.graph &&
head sample.graph || exit 1
# EXPECTED:
# - x86: high number of samples, most unresolved
# - ppc64: no samples
echo "7 - FAIL"

### 8 ###
# bug#2 fail to resolve symbols in shared libs
elif [ $t -eq 8 ]; then
set -x
EV=$INSTR_COMPLETED
cd /tmp &&
rm -rf $EV sample.* &&
$PMCSTAT -n $((0x1000000)) -d -P $EV -O sample.out /tmp/mystress &&
$PMCSTAT -R sample.out -G sample.graph &&
head sample.graph || exit 1
echo "8 - FAIL"

### 9 ###
# bug#3 fail to handle fork
elif [ $t -eq 9 ]; then
set -x
EV=$INSTR_COMPLETED
cd /tmp &&
rm -rf $EV sample.* &&
$PMCSTAT -n $((0x1000000)) -d -P $EV -O sample.out /tmp/stress.static -c 1 -t 5 &&
$PMCSTAT -R sample.out -G sample.graph &&
head sample.graph || exit 1
echo "9 - FAIL"

else
	echo "Invalid test: $t"
	exit 1
fi

# If callgraph capture is not desired use:
# pmcstat -N -S instructions -O /tmp/sample.out
# OK

# x86
# br_misp_retired.all_branches
