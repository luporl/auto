# init 1 / boot -s
service netif start
service sshd start
mount -t msdosfs -r /dev/nda0p1 /boot
mount -t tmpfs tmpfs /tmp

export LD_LIBRARY_PATH=/tmp
# pmccontrol -L

pmcstat -S CYCLES -O /tmp/cycles.pmc &

# test
pmcstat -c 0 -n $((65536*4096)) -S CYCLES -O /tmp/cycles.tmp
pmcstat -c 0 -n $((65536*32768)) -S CYCLES -O /tmp/cycles.tmp

# With trace enabled, run this several times,
# until an intr occurs after stop is called but
# before the pmc is set to none.
# The interrupt handler should be able to handle it.
# When it does, run it again and check that interrupts
# still work.
pmcstat -c 0 -n $((0x2000000)) -S CYCLES ps aux >/dev/null

sysctl kern.hwpmc.debugflags="md=allocate,config,init,intr,read,release,start,stop,write"
