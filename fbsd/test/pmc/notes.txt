### enable PMC debug prints

# ktr
options KTR
options KTR_COMPILE=(KTR_DEV|KTR_SUBSYS)
options KTR_MASK=(KTR_DEV|KTR_SUBSYS)
options KTR_CPUMASK=0xFFFFFFFFFFFFFFFF,0xFFFFFFFFFFFFFFFF,0xFFFFFFFFFFFFFFFF,0xFFFFFFFFFFFFFFFF
options KTR_ENTRIES=65535
# ## save log to disk
options ALQ
options KTR_ALQ

options         HWPMC_DEBUG             # Necessary kernel hooks for hwpmc(4)
