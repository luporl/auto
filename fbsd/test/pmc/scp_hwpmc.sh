OBJDIR=/usr/obj/usr/src/powerpc.powerpc64

scp $OBJDIR/sys/modules/hwpmc/hwpmc.ko \
	$OBJDIR/lib/libpmc/libpmc.so.5 \
	$OBJDIR/usr.sbin/pmcstat/pmcstat.full \
	$HOME/tpmc/* \
	talos2:/tmp/

#scp /usr/obj/usr/src/powerpc.powerpc64/sys/MYKERNEL/modules/usr/src/sys/modules/hwpmc/hwpmc.ko talos2:/tmp/
