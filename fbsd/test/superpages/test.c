#include <ctype.h>
#include <fcntl.h>
#include <signal.h>
#include <setjmp.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sys/mman.h>
#include <sys/sysctl.h>
#include <sys/types.h>

#include "ktest.h"

/* defs */

#define	PAGE_SIZE	4096
#define	SP_SIZE		(4096*PAGE_SIZE)

#define	TRUE		1
#define	FALSE		0

#define	T_OK		0
#define	T_FAIL		1

/* promote */
#define	F_PRO		0
#define	F_PRW		1
/* open */
#define	F_ORO		0
#define	F_ORW		8

#define	F_RO		(F_ORO|F_PRO)
#define	F_RW		(F_ORW|F_PRW)
#define	F_ORW_PRO	(F_ORW|F_PRO)	/* open rw, promote ro */
#define	F_DBG		2
#define	F_ANON		4

#define	P_SP_WRFAULT	1
#define	P_WRFAULT	2

#define	PRINTFLUSH(fmt, ...)						\
	do {								\
		printf(fmt, ## __VA_ARGS__);				\
		fflush(stdout);						\
	} while (0)

#define	FLOG(fmt, ...)							\
	do {								\
		printf("%s: " fmt "\n", __func__, ## __VA_ARGS__);	\
		fflush(stdout);						\
	} while (0)

#define	CHECK_COND(cond)						\
	do {								\
		if (!(cond)) {						\
			PRINTFLUSH("%s:%d: CHECK_COND(" #cond		\
			    ") failed\n",				\
			    __func__, __LINE__);			\
			if (kt != 0)					\
				ktest_fini();				\
			return (T_FAIL);				\
		}							\
	} while (0)

#define	CHECK_RC(a)	CHECK_COND(a == T_OK)

/* types */

typedef unsigned long ulong;

/* data */

static const char *prog;

static int kt;

static ulong sp_promotions0;
static ulong sp_promotions1;
static ulong sp_promotions;
static ulong sp_mappings0;
static ulong sp_mappings1;
static ulong sp_mappings;
static ulong sp_demotions0;
static ulong sp_demotions1;
static ulong sp_demotions;

static int refmod_limit = 32;

/* helpers */

static int
sysctl_int(const char *id)
{
	int v;
	size_t len = sizeof(v);

	if (sysctlbyname(id, &v, &len, NULL, 0) == -1) {
		perror("sysctl_int");
		exit(1);
	}
	return (v);
}

static void
sysctl_int_set(const char *id, int v)
{
	if (sysctlbyname(id, NULL, NULL, &v, sizeof(v)) == -1) {
		perror("sysctl_int_set");
		exit(1);
	}
}

static unsigned long
sysctl_ulong(const char *id)
{
	unsigned long v;
	size_t len = sizeof(v);

	if (sysctlbyname(id, &v, &len, NULL, 0) == -1) {
		perror("sysctl_ulong");
		exit(1);
	}
	return (v);
}

/* enable/disable sp_dbg */
#ifdef HAS_SP_DBG
static void
sp_dbg(int v)
{
	if (sysctlbyname("vm.pmap.sp_dbg", NULL, NULL, &v, sizeof(v)) == -1) {
		perror("sysctl failed");
		exit(1);
	}
}
#else
#define	sp_dbg(a)
#endif

static void
ktest_ini(void)
{
	if (system("kldload ./ktest.ko")) {
		FLOG("failed to load ktest");
		exit(1);
	}

	if ((kt = open("/dev/ktest", O_RDONLY)) == -1) {
		perror("open ktest");
		exit(1);
	}
}

static void
ktest_fini(void)
{
	close(kt);

	if (system("kldunload ktest")) {
		FLOG("failed to unload ktest");
		exit(1);
	}

	kt = 0;
}

static void
sp_stats0(void)
{
	sp_promotions0 = sysctl_ulong("vm.pmap.sp.promotions");
	sp_mappings0 = sysctl_ulong("vm.pmap.sp.mappings");
	sp_demotions0 = sysctl_ulong("vm.pmap.sp.demotions");
}

static void
sp_stats1(void)
{
	sp_promotions1 = sysctl_ulong("vm.pmap.sp.promotions");
	sp_mappings1 = sysctl_ulong("vm.pmap.sp.mappings");
	sp_demotions1 = sysctl_ulong("vm.pmap.sp.demotions");

	sp_promotions = sp_promotions1 - sp_promotions0;
	sp_demotions = sp_demotions1 - sp_demotions0;
	sp_mappings = sp_mappings1 - sp_mappings0;
}

static void
sp_stats_print(void)
{
	PRINTFLUSH("promotions: %lu -> %lu\n",
	    sp_promotions0, sp_promotions1);
	PRINTFLUSH("mappings: %lu -> %lu\n",
	    sp_mappings0, sp_mappings1);
	PRINTFLUSH("demotions: %lu -> %lu\n",
	    sp_demotions0, sp_demotions1);
}

static int
sp_no_changes(void)
{
	if (sp_promotions == 0 && sp_demotions == 0 && sp_mappings == 0)
		return (TRUE);
	return (FALSE);
}

static int
sp_promote_check(const char *func)
{
	int rc;

	if (sp_promotions1 > sp_promotions0) {
		rc = T_OK;
		PRINTFLUSH("%s: OK (promote)\n", func);
	} else if (sp_mappings1 > sp_mappings0) {
		rc = T_OK;
		PRINTFLUSH("%s: OK (enter)\n", func);
	} else {
		rc = T_FAIL;
		PRINTFLUSH("%s: FAIL\n", func);
	}

	return (rc);
}

static int
sp_promote_fini(const char *func)
{
	sp_stats1();
	sp_stats_print();
	return (sp_promote_check(func));
}

static void *
sp_mmap_promote(int flags, int *fd)
{
	char *m;
	int sum = 0;
	size_t i, n;
	int anon = flags & F_ANON;
	int orw = flags & F_ORW;
	int prw = flags & F_PRW;

	sp_stats0();

	/* need to open a 16MB mmap file in cur dir */
	if (anon)
		*fd = -1;
	else {
		*fd = open("mmap", orw ? O_RDWR : O_RDONLY);
		if (*fd == -1) {
			perror("open failed");
			return (NULL);
		}
	}

	/* map 1 aligned 16MB page */
	n = 16 * 1024 * 1024;
	m = mmap(0, n, PROT_READ | (orw? PROT_WRITE : 0),
		(anon? MAP_ANON : MAP_SHARED) |
		MAP_ALIGNED_SUPER, *fd, 0);
	FLOG("m=%p", m);

	if (m == MAP_FAILED) {
		perror("mmap failed");
		goto err;
	}
	/* if rw, init bytes */
	if (prw)
		/* rw promote happens when last page is written here */
		memset(m, 1, n);

	/* sum bytes */
	for (i = 0; i < n; i++)
		/* ro promote happens when last page is read here */
		sum += m[i];
	FLOG("sum=%d", sum);

	sp_stats1();
	if (sp_promote_check(__func__) != T_OK) {
		sp_stats_print();
		return (NULL);
	}
	return (m);

err:
	if (*fd != -1)
		close(*fd);
	return (NULL);
}

static int
dump_refs(char *m)
{
	static char refs[4096];
	int i, n;
	struct kt_data ktd;

	memset(&ktd, 0, sizeof(ktd));
	ktd.mem = m;
	ktd.buf = refs;
	ktd.bufsz = sizeof(refs);
	if (ioctl(kt, KT_GREFS, &ktd) == -1) {
		perror("dump_refs: ioctl failed");
		exit(1);
	}

	n = 0;
	for (i = 0; i < sizeof(refs); i++)
		if (refs[i]) {
			if (n < 16)
				printf("REF: %p\n", m + i * 4096);
			else if (n == 16)
				printf("REF: ...\n");
			n++;
		}

	return (n);
}

static int
dump_mods(char *m)
{
	static char mods[4096];
	int i, n;
	struct kt_data ktd;

	memset(&ktd, 0, sizeof(ktd));
	ktd.mem = m;
	ktd.buf = mods;
	ktd.bufsz = sizeof(mods);
	if (ioctl(kt, KT_GMODS, &ktd) == -1) {
		perror("dump_mods: ioctl failed");
		exit(1);
	}

	n = 0;
	for (i = 0; i < sizeof(mods); i++)
		if (mods[i]) {
			if (n < 16)
				printf("MOD: %p\n", m + i * 4096);
			else if (n == 16)
				printf("MOD: ...\n");
			n++;
		}

	return (n);
}

/* tests */

/* test sp promote rw with sbrk */
static int
sp_promote_sbrk(int flags)
{
	char *m;
	int sum = 0;
	intptr_t i, j, n;

	FLOG("START");
	sp_stats0();

	/* alloc 3*16MB to compensate for unaligned memory */
	n = 3 * 16 * 1024 * 1024;
	m = sbrk(n);
	if (m == (char*)-1) {
		FLOG("sbrk failed");
		return (T_FAIL);
	}
	/* set all bytes to 1 */
	memset(m, 1, n);
	/* read and sum all bytes */
	for (i = 0, j = 0; i < n; i++, j++) {
		if (j == 4 * 1024) {
			j = 0;
			if (((uintptr_t)&m[i] & 0xffffffUL) == 0xfff000UL)
				FLOG("i=%ld, m=%p", i / (1024 * 1024), &m[i]);
		}
		sum += m[i];
	}
	/* sum should be equal to 16M */
	FLOG("sum=%d", sum);
	sp_stats1();

	sbrk(-n);

	sp_stats_print();
	return (sp_promote_check(__func__));
}

/* test sp promote with mmap */
static int
sp_promote(int flags)
{
	char *m;
	int fd, rc, sum = 0;
	size_t i, j, n;

	FLOG("START: flags=%x", flags);
	sp_stats0();

	/* need to open a 16MB mmap file in cur dir */
	fd = -1;
	fd = open("mmap", flags & F_RW ? O_RDWR : O_RDONLY);
	if (fd == -1) {
		perror("open failed");
		return (T_FAIL);
	}

	/* map 1 aligned 16MB page */
	n = 16 * 1024 * 1024;
	if (flags & F_DBG)
		sp_dbg(1);

	//m = mmap(0, n, PROT_READ, MAP_ANON | MAP_ALIGNED_SUPER, fd, 0);
	m = mmap(0, n, PROT_READ | (flags & F_RW? PROT_WRITE : 0),
			MAP_SHARED | MAP_ALIGNED_SUPER, fd, 0);
	FLOG("m=%p", m);

	if (m == MAP_FAILED) {
		perror("mmap failed");
		goto err;
	}
	/* if rw, init bytes */
	if (flags & F_RW)
		/* rw promote happens when last page is written here */
		memset(m, 1, n);
	/* sum bytes */
	for (i = 0, j = 0; i < n; i++, j++) {
		if (j == 4 * 1024) {
			j = 0;
			if (((uintptr_t)&m[i] & 0xffffffUL) == 0xfff000UL)
				FLOG("i=%ld, m=%p", i / (1024 * 1024), &m[i]);
		}
		/* ro promote happens when last page is read here */
		sum += m[i];
	}

	if (flags & F_DBG)
		sp_dbg(0);

	FLOG("sum=%d", sum);
	sp_stats1();

	if (fd != -1) {
		fsync(fd);
		CHECK_RC(munmap(m, n));
		close(fd);
	}

	rc = sp_promote_check(__func__);
	if (rc != T_OK)
		sp_stats_print();
	return (rc);

err:
	if (fd != -1)
		close(fd);
	return (T_FAIL);
}

static int
sp_wire(int flags)
{
	char *m;
	int fd = -1, sum = 0;
	size_t i, n;

	FLOG("START");

	ktest_ini();

	sp_stats0();

	/* map 1 aligned 16MB page */
	n = 16 * 1024 * 1024;
	m = mmap(0, n, PROT_READ, MAP_ANON | MAP_ALIGNED_SUPER, fd, 0);
	FLOG("m=%p", m);

	if (m == MAP_FAILED) {
		perror("mmap failed");
		goto fail;
	}
	/* sum bytes */
	for (i = 0; i < n; i++)
		sum += m[i];
	/* promoted */
	FLOG("sum=%d", sum);

	CHECK_RC(sp_promote_fini(__func__));

	/* sp wire */
	FLOG("sp wire");
	sp_stats0();
	mlock(m, n);
	sp_stats1();
	CHECK_COND(sp_demotions == 1);
	CHECK_COND(sp_promotions == 1);

	FLOG("sp unwire");
	/* has some refs */
	CHECK_COND(dump_refs(m) > 0);
	sp_stats0();
	munlock(m, n);
	sp_stats1();
	/* no write perm, refs preserved */
	CHECK_COND(dump_refs(m) > 0);
	CHECK_COND(sp_no_changes());

	/* demote and unwire */
	FLOG("sp wire");
	sp_stats0();
	mlock(m, n);
	sp_stats1();
	CHECK_COND(sp_demotions == 1);
	CHECK_COND(sp_promotions == 1);

	FLOG("sp unwire");
	sp_stats0();
	munlock(m + PAGE_SIZE, 8 * PAGE_SIZE);
	sp_stats1();
	CHECK_COND(sp_promotions == 0);
	CHECK_COND(sp_mappings == 0);
	CHECK_COND(sp_demotions == 1);

	CHECK_RC(munmap(m, n));

	ktest_fini();

	FLOG("OK");
	return (T_OK);

fail:
	ktest_fini();
	return (T_FAIL);
}

static int
sp_demote(int flags)
{
	char *m;
	int fd;

	FLOG("START");

	m = sp_mmap_promote(F_RW, &fd);
	if (!m)
		return (T_FAIL);

	sp_stats0();
	fsync(fd);
	sp_stats1();
	CHECK_COND(sp_demotions == 1);

	CHECK_RC(munmap(m, SP_SIZE));
	close(fd);
	FLOG("OK");
	return (T_OK);
}

static int
sp_enter(int flags)
{
	ulong mappings, promotions;

	FLOG("START");
	sp_stats0();
	mappings = sp_mappings0;
	promotions = sp_promotions0;

	CHECK_RC(sp_promote(F_RO | F_DBG));
	CHECK_RC(sp_promote(F_RW | F_DBG));

	sp_stats0();
	CHECK_COND(sp_mappings0 > mappings &&
		sp_mappings0 - mappings > sp_promotions0 - promotions);
	FLOG("OK");
	return (T_OK);
}

static int
sp_remove_write(int flags)
{
	char *m;
	int fd;

	FLOG("START");

	m = sp_mmap_promote(F_RW, &fd);
	if (!m)
		return (T_FAIL);

	FLOG("fsync");
	sp_stats0();
	fsync(fd);
	sp_stats1();
	CHECK_COND(sp_demotions == 1);

	CHECK_RC(munmap(m, SP_SIZE));
	close(fd);
	FLOG("OK");
	return (T_OK);
}

static int
sp_memattr(int flags)
{
	char *m, *p, c;
	int fd;
	ssize_t rc;
	size_t i, n;
	struct kt_data ktd;

	FLOG("START");

	ktest_ini();

	m = sp_mmap_promote(F_RW, &fd);
	if (!m)
		goto fail;

	/* set/read */
	p = m + PAGE_SIZE;
	n = 32;
	FLOG("read");
	sp_stats0();

	memset(&ktd, 0, sizeof(ktd));
	ktd.mem = p;
	ktd.buf = p;
	ktd.bufsz = n;
	rc = ioctl(kt, KT_SMATTR, &ktd);

	sp_stats1();
	if (rc == -1) {
		perror("ioctl error");
		goto fail;
	}

	/* dump */
	for (i = 0; i < n; i++) {
		c = p[i];
		printf("%c", isprint(c)? c : '.');
		if (i < 10)
			CHECK_COND(c == '0' + i);
		else if (i < 16)
			CHECK_COND(c == 'A' + i - 10);
		else
			CHECK_COND(c == 0);
	}
	printf("\n");

	CHECK_COND(sp_demotions == 1);

	CHECK_RC(munmap(m, SP_SIZE));
	fsync(fd);
	close(fd);

	ktest_fini();

	FLOG("OK");
	return (T_OK);

fail:
	ktest_fini();
	return (T_FAIL);
}

static jmp_buf jbuf;

static void
sp_signal(int sig)
{
	longjmp(jbuf, 1);
}

static int
sp_protect(int flags)
{
	char *m, *p;
	int fd = -1;
	void (*oldsig)(int);
	int val;

	FLOG("START");

	ktest_ini();

	oldsig = signal(SIGSEGV, sp_signal);
	if (oldsig == SIG_ERR) {
		perror("signal");
		goto fail;
	}

	m = sp_mmap_promote(F_RW, &fd);
	if (!m)
		goto fail;

	/* sp protect */
	FLOG("sp protect");
	/* should have ref and mod bits set */
	CHECK_COND(dump_refs(m) > 0);
	CHECK_COND(dump_mods(m) > 0);
	sp_stats0();
	CHECK_RC(mprotect(m, SP_SIZE, PROT_READ));
	sp_stats1();
	/* ref and mod bits should have been cleared */
	CHECK_COND(dump_refs(m) == 0);
	CHECK_COND(dump_mods(m) == 0);
	CHECK_COND(sp_demotions == 0);

	val = setjmp(jbuf);
	if (val) {
		CHECK_RC(munmap(m, SP_SIZE));
		fsync(fd);
		close(fd);
		signal(SIGSEGV, oldsig);
		ktest_fini();
		FLOG("OK (caught SIGSEGV)");
		return (T_OK);
	}

	if (flags & P_SP_WRFAULT) {
		/* cause fault */
		FLOG("memset %p (should fault)", m + PAGE_SIZE);
		memset(m + PAGE_SIZE, 0xab, PAGE_SIZE);
		goto fail;
	}

	/* demote and protect */
	FLOG("sp unprotect");
	CHECK_RC(mprotect(m, SP_SIZE, PROT_READ | PROT_WRITE));

	FLOG("protect");
	sp_stats0();
	CHECK_RC(mprotect(m, 3 * PAGE_SIZE, PROT_READ));
	sp_stats1();
	CHECK_COND(sp_demotions == 1);

	/* should be ok */
	p = m + 3 * PAGE_SIZE;
	FLOG("memset %p", p);
	memset(p, 0xbc, PAGE_SIZE);

	/* should fault */
	if (flags & P_WRFAULT) {
		p = m;
		FLOG("memset %p (should fault)", m);
		memset(p, 0xbc, PAGE_SIZE);
		goto fail;
	}

	CHECK_RC(munmap(m, SP_SIZE));
	fsync(fd);
	close(fd);
	signal(SIGSEGV, oldsig);

	ktest_fini();

	FLOG("OK");
	return (T_OK);

fail:
	ktest_fini();
	return (T_FAIL);
}

static int
sp_remove(int flags)
{
	char *m;
	int fd = -1;

	FLOG("START");

	m = sp_mmap_promote(F_RO, &fd);
	CHECK_COND(m != NULL);

	FLOG("sp remove");
	sp_stats0();
	CHECK_RC(munmap(m, SP_SIZE));
	sp_stats1();
	close(fd);
	CHECK_COND(sp_demotions == 0);

	m = sp_mmap_promote(F_RO, &fd);
	FLOG("remove");
	sp_stats0();
	CHECK_RC(munmap(m + PAGE_SIZE, 8 * PAGE_SIZE));
	sp_stats1();
	CHECK_RC(munmap(m, SP_SIZE));
	close(fd);
	CHECK_COND(sp_demotions == 1);

	FLOG("OK");
	return (T_OK);
}

static void
sp_use(char *m)
{
	volatile char *v;
	int i, sum = 0;

	/* ref */
	v = m;
	for (i = 0; i < 16 * PAGE_SIZE; i++)
		sum += v[i];
	FLOG("sum=%d", sum);
}

static int
sp_ref(int flags)
{
	char *m;
	int fd = -1, oldv;
	const char *id = "vm.pageout_update_period";

	FLOG("START");

	ktest_ini();

	m = sp_mmap_promote(F_RO | F_ANON, &fd);
	CHECK_COND(m != NULL);

	oldv = sysctl_int(id);
	sysctl_int_set(id, 2);

	sp_dbg(1);

	sleep(2);
	FLOG("initial");
	CHECK_COND(dump_refs(m) == 0);

	FLOG("after use");
	sp_use(m);
	CHECK_COND(dump_refs(m) > 0);
	/* check cache */
	/*
	 * XXX call dump_refs() 1 time only because it takes a long time
	 * to finish, and by then ref bits will most likely be clear.
	 *
	 * CHECK_COND(dump_refs(m) > 0);
	 */

	FLOG("sleep");
	sleep(2);
	FLOG("after idle");
	CHECK_COND(dump_refs(m) == 0);

	sp_dbg(0);

	sysctl_int_set(id, oldv);
	CHECK_RC(munmap(m, SP_SIZE));

	ktest_fini();

	FLOG("OK");
	return (T_OK);
}

static int
sp_chg(int flags)
{
	char *m;
	int fd = -1;

	FLOG("START");

	ktest_ini();

	/* promote */
	m = sp_mmap_promote(F_RW, &fd);
	CHECK_COND(m != NULL);

	/* test */
	CHECK_COND(dump_mods(m) == refmod_limit);
	/* check cache */
	CHECK_COND(dump_mods(m) > 0);
	fsync(fd);
	CHECK_COND(dump_mods(m) == 0);

	/* unmap/close */
	CHECK_RC(munmap(m, SP_SIZE));
	close(fd);

	ktest_fini();

	FLOG("OK");
	return (T_OK);
}

static int
sp_clear_mod(int flags)
{
	char *m;
	int fd = -1;
	struct kt_data ktd;

	FLOG("START");

	ktest_ini();

	/* promote */
	m = sp_mmap_promote(F_RW | F_ANON, &fd);
	CHECK_COND(m != NULL);

	CHECK_COND(dump_mods(m) == refmod_limit);

	memset(&ktd, 0, sizeof(ktd));
	ktd.mem = m;
	if (ioctl(kt, KT_CLRMOD, &ktd) == -1) {
		perror("clear_mod: ioctl failed");
		exit(1);
	}

	CHECK_COND(dump_mods(m) < refmod_limit);

	/* repromote */
	sp_stats0();
	memset(m, 41, PAGE_SIZE);
	sp_stats1();
	CHECK_RC(sp_promote_check(__func__));

	/* unmap/close */
	CHECK_RC(munmap(m, SP_SIZE));

	ktest_fini();

	FLOG("OK");
	return (T_OK);
}

static int
sp_all(int flags)
{
	CHECK_RC(sp_promote(F_RO | F_DBG));
	CHECK_RC(sp_promote(F_RW | F_DBG));
	CHECK_RC(sp_promote_sbrk(0));
	CHECK_RC(sp_wire(0));
	CHECK_RC(sp_demote(0));
	CHECK_RC(sp_enter(0));
	CHECK_RC(sp_remove_write(0));
	CHECK_RC(sp_memattr(0));
	CHECK_RC(sp_protect(0));
	CHECK_RC(sp_protect(P_WRFAULT));
	CHECK_RC(sp_protect(P_SP_WRFAULT));
	CHECK_RC(sp_remove(0));
	CHECK_RC(sp_ref(0));
	CHECK_RC(sp_chg(0));
	CHECK_RC(sp_clear_mod(0));
	return (T_OK);
}

struct strfn {
	const char *str;
	int (*fn)(int);
	int flags;
};

static struct strfn g_strfn_map[] = {
	{ "promote_dro", sp_promote, F_RO | F_DBG },
	{ "promote_drw", sp_promote, F_RW | F_DBG },
	{ "promote_ro", sp_promote, F_RO },
	{ "promote_rw", sp_promote, F_RW },
	{ "promote_sbrk", sp_promote_sbrk, 0 },
	{ "wire", sp_wire, 0 },
	{ "demote", sp_demote, 0 },
	{ "enter", sp_enter, 0 },
	{ "remwr", sp_remove_write, 0 },
	{ "memattr", sp_memattr, 0 },
	{ "prot", sp_protect, 0 },
	{ "prot_wf", sp_protect, P_WRFAULT },
	{ "prot_swf", sp_protect, P_SP_WRFAULT },
	{ "rem", sp_remove, 0 },
	{ "ref", sp_ref, 0 },
	{ "chg", sp_chg, 0 },
	{ "clear_mod", sp_clear_mod, 0 },
	{ "all", sp_all, 0 },

	/* shortcuts */
	{ "d", sp_promote, F_RO | F_DBG },
	{ "D", sp_promote, F_RW | F_DBG },
	{ "r", sp_promote, F_RO },
	{ "w", sp_promote, F_RW },
	{ "s", sp_promote_sbrk, 0 },
	{ NULL, NULL, 0 }
};

static void
help(void)
{
	struct strfn *sf;

	for (sf = g_strfn_map; sf->str; sf++)
		printf("%s\n", sf->str);
	exit(0);
}

static void
usage(void)
{
	printf("usage: %s <test>|--help|-h\n", prog);
	exit(1);
}

int main(int argc, char **argv)
{
	struct strfn *sf;
	const char *arg;
	int rc;

	prog = argv[0];

	if (argc != 2)
		usage();
	arg = argv[1];

	if (strcmp(arg, "--help") == 0 || strcmp(arg, "-h") == 0)
		help();

	for (sf = g_strfn_map; sf->str; sf++)
		if (strcmp(arg, sf->str) == 0)
			break;
	if (sf->str == NULL)
		usage();

	FLOG("SUPERPAGES");
	rc = (*sf->fn)(sf->flags);

	if (rc == T_OK)
		FLOG("OK");
	else
		FLOG("FAIL");
	return (0);
}
