#include <sys/param.h>

#include <sys/conf.h>
#include <sys/kernel.h>
#include <sys/module.h>
#include <sys/proc.h>
#include <sys/systm.h>
#include <sys/uio.h>

#include <vm/vm.h>
#include <vm/pmap.h>
#include <vm/vm_map.h>
#include <vm/vm_page.h>

#include "ktest.h"

#define	SP_PAGES	4096
#define	SP_SIZE		(SP_PAGES*PAGE_SIZE)

static char kt_tdata[256] = "0123456789ABCDEF";
static char kt_refs[SP_PAGES];
static char kt_mods[SP_PAGES];
static int  kt_refmod_limit = 32;

static int
kt_set_memattr(struct kt_data *ktd, vm_page_t m)
{
	size_t amt;
	int error = 0;
	vm_memattr_t ma;

	uprintf("%s: mem=%p, buf=%p, bufsz=%lu, m=%p\n",
	    __func__, ktd->mem, ktd->buf, ktd->bufsz, m);

	/* set memattr */
	ma = pmap_page_get_memattr(m);
	ma |= VM_MEMATTR_UNCACHEABLE;
	pmap_page_set_memattr(m, ma);

	/* return some dummy data */
	while (ktd->bufsz > 0) {
		amt = MIN(sizeof(kt_tdata), ktd->bufsz);
		if ((error = copyout(kt_tdata, ktd->buf, amt)) != 0) {
			uprintf("%s: copyout failed!\n", __func__);
			break;
		}
		ktd->bufsz -= amt;
	}

	/* unset memattr */
	ma &= ~VM_MEMATTR_UNCACHEABLE;
	pmap_page_set_memattr(m, ma);

	return (error);
}

static int
kt_get_refs(struct kt_data *ktd, vm_page_t m)
{
	vm_page_t m_end;
	int error, i;

	/* get first pages' is_referenced() results */
	for (i = 0, m_end = &m[SP_PAGES]; m < m_end; i++, m++)
		if (i < kt_refmod_limit)
			kt_refs[i] = pmap_is_referenced(m);
		else
			kt_refs[i] = 0;

	/* return refs */
	if (ktd->bufsz < sizeof(kt_refs)) {
		uprintf("%s: buffer too small\n", __func__);
		return (EINVAL);
	}

	if ((error = copyout(kt_refs, ktd->buf, sizeof(kt_refs))) != 0)
		uprintf("%s: copyout failed!\n", __func__);

	return (error);
}

static int
kt_get_mods(struct kt_data *ktd, vm_page_t m)
{
	vm_page_t m_end;
	int error, i;

	/* get first pages' is_modified() results */
	for (i = 0, m_end = &m[SP_PAGES]; m < m_end; i++, m++)
		if (i < kt_refmod_limit)
			kt_mods[i] = pmap_is_modified(m);
		else
			kt_mods[i] = 0;

	/* return mods */
	if (ktd->bufsz < sizeof(kt_mods)) {
		uprintf("%s: buffer too small\n", __func__);
		return (EINVAL);
	}

	if ((error = copyout(kt_mods, ktd->buf, sizeof(kt_mods))) != 0)
		uprintf("%s: copyout failed!\n", __func__);

	return (error);
}

static int
kt_clear_mod(vm_page_t m)
{
	vm_page_busy_acquire(m, VM_ALLOC_NORMAL);
	pmap_clear_modify(m);
	vm_page_xunbusy(m);
	return (0);
}

static int
kt_ioctl(struct cdev *dev __unused, unsigned long cmd, caddr_t arg, int flag,
    struct thread *td)
{
	int error;
	struct kt_data *ktd;
	vm_offset_t va;
	vm_paddr_t pa;
	vm_page_t m;
	pmap_t pmap;

	pmap = &td->td_proc->p_vmspace->vm_pmap;

	ktd = (struct kt_data *)arg;

	va = (vm_offset_t)ktd->mem;
	/* get pa */
	pa = pmap_extract(pmap, va);
	/* get m */
	m = PHYS_TO_VM_PAGE(pa);

	switch (cmd) {
	case KT_SMATTR:
		error = kt_set_memattr(ktd, m);
		break;

	case KT_CLRMOD:
		error = kt_clear_mod(m);
		break;

	case KT_GREFS:
		error = kt_get_refs(ktd, m);
		break;

	case KT_GMODS:
		error = kt_get_mods(ktd, m);
		break;

	default:
		error = ENOTTY;
	}

	return (error);
}

static struct cdevsw kt_cdevsw = {
	.d_version = D_VERSION,
	.d_ioctl = kt_ioctl,
	.d_name = "ktest",
};

static struct cdev *kt_dev;

static int
kt_loader(struct module *m, int what, void *arg)
{
	int error;

	switch (what) {
		case MOD_LOAD:
			error = make_dev_p(MAKEDEV_CHECKNAME | MAKEDEV_WAITOK,
			    &kt_dev,
			    &kt_cdevsw,
			    0,
			    UID_ROOT,
			    GID_WHEEL,
			    0600,
			    "ktest");
			if (error != 0)
				return (error);

			uprintf("ktest loaded\n");
			break;

		case MOD_UNLOAD:
			destroy_dev(kt_dev);
			uprintf("ktest unloaded\n");
			break;

		default:
			return (EOPNOTSUPP);
	}

	return (0);
}

DEV_MODULE(ktest, kt_loader, NULL);
