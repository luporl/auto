#include <sys/ioccom.h>

struct kt_data {
	void *mem;
	void *buf;
	size_t bufsz;
};

#define	KT_SMATTR	_IOW('k', 1, struct kt_data)
#define	KT_CLRMOD	_IOW('k', 2, struct kt_data)
#define	KT_GREFS	_IOW('k', 3, struct kt_data)
#define	KT_GMODS	_IOW('k', 4, struct kt_data)
