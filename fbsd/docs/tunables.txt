# disable SMP
kern.smp.disabled=1

unset usefdt
hw.ofwfb.disable=1
machdep.ofw.mtx_spin=1
debug.quiesce_ofw=0
hint.xhci.0.disabled=1
hint.vscsi.0.disabled=1

debug.verbose_sysinit

cas=0
