#!/bin/csh

if ($#argv < 1) then
	goto usage
endif

set action = $argv[1]
set JAIL  = head
set VER   = 13.0-CURRENT
set PORTS = default
set J     = 32

switch ($action)
case deljail:
	sudo poudriere jail -d -j $JAIL
	sudo rm -rf /usr/jail
	sudo chflags -R 0 /usr/jail
	sudo rm -rf /usr/jail
	breaksw
case delports:
	sudo poudriere ports -d -p $PORTS
	breaksw
case deldata:
	sudo rm -rf `cat poudriere.conf | grep BASEFS | cut -d= -f2`
	breaksw
case installjail:
	goto installjail
case installjailv1:
	unset LIB32LD
	setenv WITHOUT_CROSS_COMPILER 1
	setenv WITHOUT_CLANG 1
	setenv WITHOUT_LLD 1
	setenv WITHOUT_LLVM_LIBUNWIND 1
	setenv WITHOUT_LLD_IS_LD 1
	goto installjail
case bootstrap:
	goto bootstrap
case conf:
	sudo cp poudriere.conf /usr/local/etc/ && \
	sudo mkdir -p /usr/local/etc/pkg/repos && \
	sudo cp local.conf /usr/local/etc/pkg/repos/ && \
	sudo cp make.conf /usr/local/etc/poudriere.d/ && \
	exit 0 || exit 1
case jail:
	sudo mkdir -p `cat poudriere.conf | grep BASEFS | cut -d= -f2`
	sudo mkdir -p `cat poudriere.conf | grep CCACHE_DIR | cut -d= -f2`
	sudo poudriere jail -c -j $JAIL -m null -M /usr/jail -v 13.0-CURRENT
	breaksw
case ports:
	sudo poudriere ports -c -p $PORTS -m null -M /usr/ports
	breaksw
case opt:
	sudo poudriere options -j $JAIL -p $PORTS -f packages-$PORTS
	breaksw
case opt1:
	sudo poudriere options -j $JAIL -p $PORTS $argv[2]
	breaksw
case bulk:
	sudo poudriere bulk -j $JAIL -p $PORTS -f packages-$PORTS
	breaksw
case bulkall:
	sudo poudriere bulk -j $JAIL -p $PORTS -a
	breaksw
case up:
	sudo poudriere ports -u
	breaksw
default:
	goto usage
endsw
	exit 0


usage:
	echo "usage: $0 deljail|delports|deldata|installjail|installjailv1"
	echo "       $0 bootstrap|conf|jail|ports|opt|opt1|bulk|bulkall|up"
	exit 1

bootstrap:
	foreach pkg (security/sudo ports-mgmt/poudriere net/rsync sysutils/tmux editors/vim-console)
		set basepkg=`basename $pkg`
		pkg info $basepkg >/dev/null
		if ($?) then
			cd /usr/ports/$pkg && make install clean || exit 1
		else
		endif
	end
	exit 0

installjail:
	cd /usr/src && \
	sudo -E make installworld DESTDIR=/usr/jail DB_FROM_SRC=1 && \
	sudo -E make distrib-dirs DESTDIR=/usr/jail DB_FROM_SRC=1 && \
	sudo -E make distribution DESTDIR=/usr/jail DB_FROM_SRC=1 && \
	exit 0 || exit 1
