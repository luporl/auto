#!/bin/sh

DIRS=`ls /usr/ports`

for d in $DIRS; do
	case "$d" in
		Keywords|Mk|Templates|Tools)
		continue
		;;
	esac

	if [ ! -d "/usr/ports/$d" ]; then
		continue
	fi

	ls /usr/ports/$d | sed "s@^@$d/&@" | tee lst/$d.lst
done
