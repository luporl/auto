#!/bin/sh

set -eu
FILE=
OUT=

usage()
{
	echo "usage: $0 local.conf|make.conf|poudriere.conf|all"
	echo
	echo "env vars:"
	echo "  CCACHE_DIR"
	echo "  JOBS - number of parallel pkg builders"
	echo "  PARALLEL - number of parallel jobs inside each jail"
	echo "  ROOT - poudriere root"
	echo "  ZFS - enable ZFS"
	exit 1
}

set_out()
{
	if [ $# -ge 1 ]; then
		OUT=$1
	else
		OUT=
	fi
}

set_parallel()
{
	if [ $PARALLEL -ge 1 ]; then
		parallel=
	else
		parallel="# "
	fi
}

gen_local()
{
	set_out $*

	cat <<EOF | tee $OUT
Poudriere: {
  url: "file://$ROOT/data/packages/head-default"
}

FreeBSD: {
  enabled: no
}
EOF
}

gen_make()
{
	set_out $*
	set_parallel $*

	cat <<EOF | tee $OUT
${parallel}MAKE_JOBS_NUMBER=$PARALLEL
EOF
}

gen_poudriere()
{
	local _jobs

	set_out $*
	set_parallel $*

	if [ $JOBS -ge 1 ]; then
		_jobs=
	else
		_jobs="# "
	fi

	if [ $ZFS -eq 1 ]; then
	   cat <<EOF | tee $OUT
ZPOOL=zfs
ZROOTFS=/poudriere
EOF
	else
	   cat <<EOF | tee $OUT
NO_ZFS=yes
EOF
	fi

	cat <<EOF | tee -a $OUT

FREEBSD_HOST=https://download.FreeBSD.org
SVN_HOST=svn.FreeBSD.org
RESOLV_CONF=/etc/resolv.conf
BASEFS=$ROOT

# Use portlint to check ports sanity
USE_PORTLINT=no

USE_TMPFS=yes

# If set the given directory will be used for the distfiles
# This allows to share the distfiles between jails and ports tree
# If this is "no", poudriere must be supplied a ports tree that already has
# the required distfiles.
DISTFILES_CACHE=/usr/ports/distfiles

# Automatic OPTION change detection
# When bulk building packages, compare the options from kept packages to
# the current options to be built. If they differ, the existing package
# will be deleted and the port will be rebuilt.
# Valid options: yes, no, verbose
# verbose will display the old and new options
CHECK_CHANGED_OPTIONS=yes

CHECK_CHANGED_DEPS=yes
#BAD_PKGNAME_DEPS_ARE_FATAL=yes

# ccache support. Supply the path to your ccache cache directory.
# It will be mounted into the jail and be shared among all jails.
# It is recommended that extra ccache configuration be done with
# ccache -o rather than from the environment.
#CCACHE_DIR=$CCACHE_DIR

${parallel}ALLOW_MAKE_JOBS=yes
${_jobs}PARALLEL_JOBS=$JOBS
# PREPARE_PARALLEL_JOBS=10

# If set, failed builds will save the WRKDIR to \${POUDRIERE_DATA}/wrkdirs
#SAVE_WRKDIR=yes

# Choose the default format for the workdir packing: could be tar,tgz,tbz,txz
# default is tbz
# WRKDIR_ARCHIVE_FORMAT=tbz

# Disable linux support
# NOLINUX=yes

# List of packages that will always be allowed to use MAKE_JOBS
# regardless of ALLOW_MAKE_JOBS. This is useful for allowing ports
# which holdup the rest of the queue to build more quickly.
#ALLOW_MAKE_JOBS_PACKAGES="pkg ccache py*"

# Make testing errors fatal.
# If set to 'no', ports with test failure will be marked as failed but still
# packaged to permit testing dependent ports (useful for bulk -t -a)
# Default: yes
PORTTESTING_FATAL=yes

# Define the building jail hostname to be used when building the packages
# Some port/packages hardcode the hostname of the host during build time
# This is a necessary setup for reproducible builds.
BUILDER_HOSTNAME=pkg.FreeBSD.org

# Define to get a predictable timestamp on the ports tree
# This is a necessary setup for reproducible builds.
PRESERVE_TIMESTAMP=yes
EOF
}

gen_all()
{
	gen_local local.conf
	gen_make make.conf
	gen_poudriere poudriere.conf
}

if [ $# -lt 1 ]; then
	usage
fi

set +u
if [ x$CCACHE_DIR = x ]; then
	CCACHE_DIR=/var/cache/ccache
fi
if [ x$JOBS = x ]; then
	JOBS=0
fi
if [ x$PARALLEL = x ]; then
	PARALLEL=0
fi
if [ x$ROOT = x ]; then
	ROOT=/usr/local/poudriere
fi
if [ x$ZFS = x ]; then
	ZFS=0
fi
set -u

case $1 in
	local.conf)
		gen_local
		;;
	make.conf)
		gen_make
		;;
	poudriere.conf)
		gen_poudriere
		;;
	all)
		gen_all
		;;
	*)
		usage
		;;
esac
