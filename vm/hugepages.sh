# Amount of allocated hugepages, in GB
echo $((`cat /proc/sys/vm/nr_hugepages` / 64))
