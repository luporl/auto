# create disk
qemu-img create -f qcow2 ~/disks/<disk_name>.qcow2 <disk_size>G

# snapshot
# -s: save
qemu-img snapshot -c <snapshot_name> <disk>
# -d: delete
qemu-img snapshot -d <snapshot_name> <disk>
# -r: restore
qemu-img snapshot -a <snapshot_name> <disk>
# -l: list
qemu-img snapshot -l <disk>

# defrag
qemu-img convert -f qcow2 -o preallocation=off -O qcow2 <src_disk> <dst_disk>
