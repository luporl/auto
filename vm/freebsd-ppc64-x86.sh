NAME="freebsd-ppc64-x86"

CPU="POWER8"
SMP=
MACHINE="pseries,cap-cfpc=broken,cap-sbbc=broken,cap-ibs=broken,cap-hpt-max-page-size=16M"
KVM=
MEM="1G"

DISK="-drive file=/home/luporl/disks/fbsd-ppc64-x86.qcow2,index=1,media=disk"
#DISK="$DISK -drive file=/home/luporl/disks/fbsd-ppc64-x86-fat.raw,index=2,media=disk"
#DISK=
CD="-drive file=/home/luporl/isos/fbsd-ppc64.iso,index=3,media=cdrom"
#CD=

HOSTNAME="fbsd-ppc64-x86"
IP_GUEST="10.0.0.64"
IP_HOST="10.0.0.2"
IP_DNS="10.0.0.3"
IP_DHCPSTART="10.0.0.64"
MAC="52:54:98:14:64:01"
NET_FWD="tcp::1464-:22"

NETBOOT=",tftp=/home/luporl/mnt,bootfile=/boot/loader"
NETBOOT=

# user
NET="-netdev user,id=lan0,ipv6=off,net=$IP_GUEST/24,host=$IP_HOST"
NET="$NET,hostname=$HOSTNAME,dhcpstart=$IP_DHCPSTART,dns=$IP_DNS"
NET="$NET,hostfwd=$NET_FWD$NETBOOT"
NET="$NET -device e1000,netdev=lan0,mac=$MAC"

# tap
NET="-net nic,model=e1000,macaddr=$MAC"
NET="$NET -net tap,ifname=tap0,script=no,downscript=no"

SNAPSHOT="-snapshot"
BOOT_NW="-boot order=n"

set -eux
ECHO=${ECHO:-}

$ECHO qemu-system-ppc64 \
    -name $NAME -cpu $CPU $SMP -machine $MACHINE $KVM -m $MEM \
    $DISK $CD $NET $SNAPSHOT $BOOT_NW \
    -nographic -vga none -serial mon:stdio -nodefaults $@
