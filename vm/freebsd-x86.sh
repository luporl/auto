NAME="freebsd-x86"

CPU="host"
SMP="-smp cores=2,threads=2"
MACHINE="pc-i440fx-2.5"
KVM="-accel kvm"
MEM="8G"

DISK="-drive file=/home/luporl/disks/fbsd-x86.qcow2,index=1,media=disk"
CD="-drive file=/home/luporl/isos/fbsd-amd64.iso,index=2,media=cdrom"

HOSTNAME="fbsd-x86"
IP_GUEST="10.0.0.14"
IP_HOST="10.0.0.2"
IP_DNS="10.0.0.3"
IP_DHCPSTART="10.0.0.14"
MAC="52:54:98:14:86:01"

NETBOOT=",tftp=/home/luporl/mnt,bootfile=/boot/loader"
NETBOOT=

NET="-netdev user,id=lan0,ipv6=off,net=$IP_GUEST/24,host=$IP_HOST"
NET="$NET,hostname=$HOSTNAME,dhcpstart=$IP_DHCPSTART,dns=$IP_DNS"
NET="$NET,hostfwd=tcp::1401-:22$NETBOOT"
NET="$NET -device e1000,netdev=lan0,mac=$MAC"

SNAPSHOT="-snapshot"
SNAPSHOT=
BOOT_NW="-boot order=n"
BOOT_NW=

set -eux
ECHO=${ECHO:-}

$ECHO qemu-system-x86_64 \
    -name $NAME -cpu $CPU $SMP -machine $MACHINE $KVM -m $MEM \
    $DISK $CD $NET $SNAPSHOT $BOOT_NW \
    -nographic -vga none -serial mon:stdio -nodefaults $@
