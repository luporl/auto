# check if netdev group exists
getent group netdev
# create it if not
sudo groupadd netdev

# check tap devices
ip tuntap show
# add new tap device
sudo ip tuntap add dev tap0 mode tap group netdev
sudo ip link set dev tap0 up

# setup bridge
sudo brctl addbr br0
sudo brctl addif br0 enp1s0
sudo brctl addif br0 tap0
