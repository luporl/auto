CPU="-cpu host -smp cores=8,threads=4"

HUGEPAGES="-mem-prealloc -mem-path /dev/hugepages"
HUGEPAGES=
MEM="-m 8G $HUGEPAGES"

KVM=",accel=kvm"

# HPT
#MACHINE="-machine pseries$KVM,cap-cfpc=broken,cap-sbbc=broken,cap-ibs=broken,cap-hpt-max-page-size=16M"
# Radix
MACHINE="-machine pseries$KVM,cap-cfpc=broken,cap-sbbc=broken,cap-ibs=broken"

DISKS="-drive file=/home/luporl/disks/fbsd-ppc64le.qcow2,index=1,media=disk"
DISKS="$DISKS -drive file=/home/luporl/disks/fbsd-ppc64le-fat.raw,index=2,media=disk"

CD="-drive file=/home/luporl/isos/fbsd-ppc64le.iso,index=3,media=cdrom"

NET="-netdev user,id=lan0,ipv6=off,net=10.0.0.65/24,host=10.0.0.2,hostname=fbsd-ppc64le,dhcpstart=10.0.0.65,dns=10.0.0.3,tftp=/home/luporl/netboot/ppc64,bootfile=/boot/loader,hostfwd=tcp::1465-:22"
NET="$NET -device e1000,netdev=lan0,mac=52:54:98:14:65:01"

qemu-system-ppc64 \
	-name freebsd-ppc64le $CPU $MEM $MACHINE \
	$DISKS $CD $NET \
	-nographic -vga none -serial mon:stdio -nodefaults
