NAME="freebsd-ppc64"

CPU="host"
SMP="-smp cores=8,threads=4"
#MACHINE="pseries,cap-cfpc=broken,cap-sbbc=broken,cap-ibs=broken,cap-hpt-max-page-size=16M"
MACHINE="pseries,cap-cfpc=broken,cap-sbbc=broken,cap-ibs=broken"
KVM="-accel kvm"
#MEM="-m 8G -mem-prealloc -mem-path /dev/hugepages"
MEM="-m 8G"

DISK_IF=
#DISK_IF=",if=virtio"

DISK="-drive file=/home/luporl/disks/fbsd-ppc64.qcow2,index=1,media=disk$DISK_IF"
DISK="$DISK -drive file=/home/luporl/disks/fbsd-ppc64-fat.raw,index=0,media=disk$DISK_IF"
#DISK="$DISK -drive file=/home/luporl/disks/fbsd-ppc64-zfs.qcow2,index=3,media=disk$DISK_IF"
CD="-drive file=/home/luporl/isos/fbsd-ppc64.iso,index=2,media=cdrom$DISK_IF"
CD=

HOSTNAME="fbsd-ppc64"
IP_GUEST="10.0.0.64"
IP_HOST="10.0.0.2"
IP_DNS="10.0.0.3"
IP_DHCPSTART="$IP_GUEST"
MAC="52:54:98:14:64:01"

NETBOOT=",tftp=/home/luporl/mnt,bootfile=/boot/loader"
NETBOOT=

NET="-netdev user,id=lan0,ipv6=off,net=$IP_GUEST/24,host=$IP_HOST"
NET="$NET,hostname=$HOSTNAME,dhcpstart=$IP_DHCPSTART,dns=$IP_DNS"
NET="$NET,hostfwd=tcp::1464-:22$NETBOOT"
NET="$NET -device e1000,netdev=lan0,mac=$MAC"

# tap
NET="-net nic,model=virtio,macaddr=$MAC"
NET="$NET -net tap,ifname=tap7,script=no,downscript=no"

SNAPSHOT="-snapshot"
#SNAPSHOT=
BOOT_NW="-boot order=n"
BOOT_NW=

set -eux
ECHO=${ECHO:-}

$ECHO qemu-system-ppc64 \
    -name $NAME -cpu $CPU $SMP -machine $MACHINE $KVM $MEM \
    $DISK $CD $NET $SNAPSHOT $BOOT_NW \
    -nographic -vga none -serial mon:stdio -nodefaults $@
