KVM=1
GDB=0

#QEMU=qemu-system-ppc64
#QEMU=/home/luporl/qemu/master/bin/qemu-system-ppc64
#QEMU="gdb --args /home/luporl/qemu/dbg/bin/qemu-system-ppc64"
QEMU="/home/luporl/qemu/master/bin/qemu-system-ppc64"

if [ "$GDB" -eq 1 ]; then
	QEMU="gdb --args $QEMU"
fi

if [ "$KVM" -eq 1 ]; then
	# KVM
	CPU="-cpu host -smp cores=8,threads=4"
	MACHINE="-machine pseries,cap-nested-hv=true"
	ACCEL="-accel kvm"
else
	# TCG
	CPU="-cpu power9"
	MACHINE="-machine pseries,cap-nested-hv=true"
	ACCEL="-accel tcg,thread=multi -smp 4"
fi

$QEMU \
	-name debian11 $CPU $MACHINE $ACCEL \
	-m 8G \
	-drive file=/home/luporl/disks/debian11-ppc64.qcow2,index=1,media=disk \
	-drive file=/home/luporl/isos/debian-11.0.0-ppc64el-netinst.iso,index=2,media=cdrom \
	-netdev user,id=lan0,ipv6=off,net=10.0.0.11/24,host=10.0.0.2,hostname=bullseye,dhcpstart=10.0.0.10,dns=10.0.0.3,hostfwd=tcp::8022-:22 \
	-device e1000,netdev=lan0,mac=52:54:00:00:00:11 \
	-nographic -vga none -serial mon:stdio -nodefaults
